import {defineStore} from 'pinia'
import {ethers, Interface} from 'ethers'
import {toRaw} from "vue";
import {useWalletsStore} from "@/stores/wallets";
import {useNativeFetch} from "~/composables/useNativeFetch.js";

export const useContractStore = defineStore('contract', {
  state: () => ({
    abi: [
      {
        "inputs": [],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "target",
            "type": "address"
          }
        ],
        "name": "AddressEmptyCode",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "AddressInsufficientBalance",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "sender",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "balance",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "needed",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "ERC1155InsufficientBalance",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "approver",
            "type": "address"
          }
        ],
        "name": "ERC1155InvalidApprover",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "idsLength",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "valuesLength",
            "type": "uint256"
          }
        ],
        "name": "ERC1155InvalidArrayLength",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          }
        ],
        "name": "ERC1155InvalidOperator",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "receiver",
            "type": "address"
          }
        ],
        "name": "ERC1155InvalidReceiver",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "sender",
            "type": "address"
          }
        ],
        "name": "ERC1155InvalidSender",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          }
        ],
        "name": "ERC1155MissingApprovalForAll",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "implementation",
            "type": "address"
          }
        ],
        "name": "ERC1967InvalidImplementation",
        "type": "error"
      },
      {
        "inputs": [],
        "name": "ERC1967NonPayable",
        "type": "error"
      },
      {
        "inputs": [],
        "name": "FailedInnerCall",
        "type": "error"
      },
      {
        "inputs": [],
        "name": "InvalidInitialization",
        "type": "error"
      },
      {
        "inputs": [],
        "name": "NotInitializing",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          }
        ],
        "name": "OwnableInvalidOwner",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "OwnableUnauthorizedAccount",
        "type": "error"
      },
      {
        "inputs": [],
        "name": "UUPSUnauthorizedCallContext",
        "type": "error"
      },
      {
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "slot",
            "type": "bytes32"
          }
        ],
        "name": "UUPSUnsupportedProxiableUUID",
        "type": "error"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "account",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "bool",
            "name": "approved",
            "type": "bool"
          }
        ],
        "name": "ApprovalForAll",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "uint64",
            "name": "version",
            "type": "uint64"
          }
        ],
        "name": "Initialized",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256[]",
            "name": "ids",
            "type": "uint256[]"
          },
          {
            "indexed": false,
            "internalType": "uint256[]",
            "name": "values",
            "type": "uint256[]"
          }
        ],
        "name": "TransferBatch",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "value",
            "type": "uint256"
          }
        ],
        "name": "TransferSingle",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "string",
            "name": "value",
            "type": "string"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          }
        ],
        "name": "URI",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "implementation",
            "type": "address"
          }
        ],
        "name": "Upgraded",
        "type": "event"
      },
      {
        "inputs": [],
        "name": "UPGRADE_INTERFACE_VERSION",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          }
        ],
        "name": "balanceOf",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address[]",
            "name": "accounts",
            "type": "address[]"
          },
          {
            "internalType": "uint256[]",
            "name": "ids",
            "type": "uint256[]"
          }
        ],
        "name": "balanceOfBatch",
        "outputs": [
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256[]",
            "name": "productIds",
            "type": "uint256[]"
          },
          {
            "internalType": "uint256[]",
            "name": "amounts",
            "type": "uint256[]"
          }
        ],
        "name": "buyAndStoreTokens",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256[]",
            "name": "collectionIds",
            "type": "uint256[]"
          },
          {
            "internalType": "uint256[]",
            "name": "tokenIds",
            "type": "uint256[]"
          }
        ],
        "name": "buyTokens",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "categories",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "commissionRate",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "name": "createCategory",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "collectionId",
            "type": "uint256"
          },
          {
            "internalType": "bytes",
            "name": "data",
            "type": "bytes"
          },
          {
            "internalType": "uint256",
            "name": "numberOfTokens",
            "type": "uint256"
          }
        ],
        "name": "createMultipleNFTs",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "uint256",
            "name": "categoryId",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "tokenPrice",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "numberOfTokens",
            "type": "uint256"
          }
        ],
        "name": "createProductCollection",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "categoryId",
            "type": "uint256"
          }
        ],
        "name": "deleteCategoryById",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "categoryId",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "newName",
            "type": "string"
          }
        ],
        "name": "editCategory",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "getAllCategories",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
              },
              {
                "internalType": "string",
                "name": "name",
                "type": "string"
              }
            ],
            "internalType": "struct VineCollectionContractV3.CategoryCollection[]",
            "name": "",
            "type": "tuple[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "collectionId",
            "type": "uint256"
          }
        ],
        "name": "getCollection",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
              },
              {
                "internalType": "string",
                "name": "name",
                "type": "string"
              },
              {
                "internalType": "address",
                "name": "owner",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "categoryId",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "tokenPrice",
                "type": "uint256"
              },
              {
                "internalType": "uint256[]",
                "name": "tokenIds",
                "type": "uint256[]"
              }
            ],
            "internalType": "struct VineCollectionContractV3.ProductCollection",
            "name": "",
            "type": "tuple"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "user",
            "type": "address"
          }
        ],
        "name": "getUserStorage",
        "outputs": [
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "initialOwner",
            "type": "address"
          }
        ],
        "name": "initialize",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          }
        ],
        "name": "isApprovedForAll",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          },
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          },
          {
            "internalType": "bytes",
            "name": "",
            "type": "bytes"
          }
        ],
        "name": "onERC1155BatchReceived",
        "outputs": [
          {
            "internalType": "bytes4",
            "name": "",
            "type": "bytes4"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "bytes",
            "name": "",
            "type": "bytes"
          }
        ],
        "name": "onERC1155Received",
        "outputs": [
          {
            "internalType": "bytes4",
            "name": "",
            "type": "bytes4"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "products",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "categoryId",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "tokenPrice",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "proxiableUUID",
        "outputs": [
          {
            "internalType": "bytes32",
            "name": "",
            "type": "bytes32"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256[]",
            "name": "ids",
            "type": "uint256[]"
          },
          {
            "internalType": "uint256[]",
            "name": "values",
            "type": "uint256[]"
          },
          {
            "internalType": "bytes",
            "name": "data",
            "type": "bytes"
          }
        ],
        "name": "safeBatchTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "value",
            "type": "uint256"
          },
          {
            "internalType": "bytes",
            "name": "data",
            "type": "bytes"
          }
        ],
        "name": "safeTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "internalType": "bool",
            "name": "approved",
            "type": "bool"
          }
        ],
        "name": "setApprovalForAll",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "newuri",
            "type": "string"
          }
        ],
        "name": "setURI",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256[]",
            "name": "tokenIds",
            "type": "uint256[]"
          }
        ],
        "name": "storeTokens",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "bytes4",
            "name": "interfaceId",
            "type": "bytes4"
          }
        ],
        "name": "supportsInterface",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "tokenToCollection",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newImplementation",
            "type": "address"
          },
          {
            "internalType": "bytes",
            "name": "data",
            "type": "bytes"
          }
        ],
        "name": "upgradeToAndCall",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "uri",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256[]",
            "name": "tokenIds",
            "type": "uint256[]"
          }
        ],
        "name": "withdrawTokens",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "stateMutability": "payable",
        "type": "receive"
      }
    ],
    contractAddress: "0x3E6b4dae78485d756Ec9F442544cA4238392b25d"
  }),
  actions: {
    async createCategory(categoryName) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        try {

          const tx = await contract.createCategory(categoryName);
          await tx.wait();
          console.log(`Category ${categoryName} created successfully`);
        } catch (error) {
          console.error("Error creating category:", error);
        }
      }
    },
    async editCategory(category) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        try {
          const tx = await contract.editCategory(category.id, category.name);
          await tx.wait();
          console.log(`Category ${category.name} edited successfully`);
        } catch (error) {
          console.error("Error creating category:", error);
        }
      }
    },
    async deleteCategory(categoryId) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        try {

          const tx = await contract.deleteCategoryById(categoryId);
          await tx.wait();
          console.log(`Category ${categoryId} deleted successfully`);
        } catch (error) {
          console.error("Error creating category:", error);
        }
      }
    },
    async getAllCategories() {
      // Create a new contract instance
      const walletsStore = useWalletsStore();
      const provider = walletsStore.provider;
      const contract = new ethers.Contract(this.contractAddress, this.abi, toRaw(provider));
      try {
        // Call the getAllCategories function
        return await contract.getAllCategories();
      } catch (error) {
        console.error('Error:', error);
      }
    },
    async createProductCollection(form) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        console.log(form.name, form.categoryId, form.tokenPrice, form.numberOfTokens)
        try {
          const tx = await contract.createProductCollection(form.name, form.categoryId, form.tokenPrice, form.numberOfTokens);
          await tx.wait();
          const receipt = await toRaw(provider).getTransactionReceipt(tx.hash);
          const iface = new Interface(this.abi)
          let blockchainId = 0;
          receipt.logs.forEach((log) => {
            const parsedLog = iface.parseLog(log);
            if (parsedLog?.name === "ProductCollectionCreated") {
              blockchainId = Number(parsedLog?.args[0]);
            }
          });

          const formData = new FormData();
          formData.append('name', form.name);
          formData.append('categoryId', form.categoryId);
          formData.append('price', form.tokenPrice);
          formData.append('quantity', form.numberOfTokens);
          formData.append('description', form.description);
          formData.append('blockchainId', blockchainId);

          if (form.image) {
            formData.append('image', form.image);
          }
          const data = await useNativeFetch('products', {
            method: 'POST',
            body: formData,
          }, {}, true, true);
          // console.log(`Collection ${collectionName} created successfully`);
        } catch (error) {
          console.error("Error creating collection:", error);
        }
      }
    },
    async updateProductCollection(blockchainId, dbId, form) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        try {
          const tx = await contract.updateProductCollection(blockchainId, form.name, form.tokenPrice, form.categoryId);
          console.log(tx)
          await tx.wait();
          const formData = new FormData();
          formData.append('name', form.name);
          formData.append('categoryId', form.categoryId);
          formData.append('price', form.tokenPrice);

          const data = await useNativeFetch(`products/${dbId}`, {
            method: 'POST',
            body: formData,
          }, {}, true, true);
          // console.log(`Collection ${collectionName} created successfully`);
        } catch (error) {
          console.error("Error creating collection:", error);
        }
      }
    },
    async addTokens(blockchainId, dbId, numberOfTokens) {
      const walletsStore = useWalletsStore();
      const provider = walletsStore.provider;
      const signer = await toRaw(provider).getSigner();
      const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
      try {
        console.log(numberOfTokens)
        const tx = await contract.createMultipleNFTs(blockchainId, '0x', numberOfTokens);
        await tx.wait();
        const data = await useNativeFetch(`products/quantity/${dbId}`, {
          method: 'PUT',
          body: {quantity: numberOfTokens},
        });
      } catch (error) {
        console.error("Error creating collection:", error);
      }
    },
    async getAllProductCollections() {
      // Create a new contract instance
      const walletsStore = useWalletsStore();
      const provider = walletsStore.provider;
      const contract = new ethers.Contract(this.contractAddress, this.abi, toRaw(provider));
      try {
        // Call the getAllCategories function
        return await contract.getAllProductCollections();
      } catch (error) {
        console.error('Error:', error);
      }
    },
    async getProductCollection(collectionId) {
      // Create a new contract instance
      const walletsStore = useWalletsStore();
      const provider = walletsStore.provider;
      const contract = new ethers.Contract(this.contractAddress, this.abi, toRaw(provider));
      try {
        // Call the getAllCategories function
        return await contract.getCollection(collectionId);
      } catch (error) {
        console.error('Error:', error);
      }
    },
    async deleteProduct(dbId, blockchainId) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        try {
          const tx = await contract.deleteProductCollectionById(blockchainId);
          await tx.wait();
          const data = await useNativeFetch(`products/${dbId}`, {
            method: 'DELETE'
          });
          console.log(`Product ${dbId} deleted successfully`);
        } catch (error) {
          console.error("Error deleting product:", error);
        }
      }
    },
    async buyTokens(products) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        const tokenIds = [];
        for (let i = 0; i < products.length; i++) {
         let collection = await contract.getCollection(products[i].blockchainId);
          for (let j = 0; j < products[i].quantity; j++) {
            tokenIds.push(collection.tokenIds[j]);
          }
        }
       const collectionIds = products.map(product => product.blockchainId);

        const totalPriceInEther = products.reduce((acc, product) => {
          return acc + (product.price * product.quantity);
        }, 0);

        const totalPrice = products.reduce((acc, product) => {
          return acc + (product.price * product.quantity);
        }, 0).toString();
        try {
          console.log(collectionIds, tokenIds);
          const tx = await contract.buyTokens(collectionIds, tokenIds, {
            value: 2
          });
          await tx.wait();
          console.log(`Tokens ${tokenIds} bought successfully`);
          return tx;

        } catch (error) {
          console.error("Error buying tokens:", error);
        }
      }
    },
    async buyAndStoreTokens(products) {
      const walletsStore = useWalletsStore();

      if (walletsStore.isMetamaskConnected) {
        const walletsStore = useWalletsStore();
        const provider = walletsStore.provider;
        const signer = await toRaw(provider).getSigner();
        const contract = new ethers.Contract(this.contractAddress, this.abi, signer);
        // const tokenIds = [];
        // for (let i = 0; i < products.length; i++) {
        //   let collection = await contract.getCollection(products[i].blockchainId);
        //   for (let j = 0; j < products[i].quantity; j++) {
        //     tokenIds.push(collection.tokenIds[j]);
        //   }
        // }
        const collectionIds = products.map(product => product.blockchainId);
        const quantity = products.map(product => product.quantity);

        const totalPrice = products.reduce((acc, product) => {
          return acc + (product.price * product.quantity);
        }, 0).toString();
        try {
          console.log(collectionIds, quantity);
          const tx = await contract.buyAndStoreTokens(collectionIds, quantity, {
            value: totalPrice
          });
          await tx.wait();
          console.log(`Tokens stored successfully`);
          return tx;

        } catch (error) {
          console.error("Error buying tokens:", error);
        }
      }
    },
  },
});