import {useNativeFetch} from "~/composables/useNativeFetch.js";

export const useUserStore = defineStore('user', {
  state: () => ({
    isAuthorized:  JSON.parse(localStorage.getItem("user"))?.isAuthorized,
    accessToken:  JSON.parse(localStorage.getItem("user"))?.accessToken,
    refreshToken:  JSON.parse(localStorage.getItem("user"))?.refreshToken,
  }),
  actions: {
    async signIn(address, signature) {
      console.log(signature)
      console.log(address)
      const data = await useNativeFetch('auth/sign/in', {
        method: 'POST',
        body: {
          address,
          signature,
        },
      });
      this.isAuthorized = true;
      this.accessToken = data.tokens.accessToken;
      this.refreshToken = data.tokens.refreshToken;
      localStorage.setItem(
        "user",
        JSON.stringify({
          isAuthorized: this.isAuthorized,
          accessToken: this.accessToken,
          refreshToken: this.refreshToken,
        })
      );
    },
    async signOut() {
      this.isAuthorized = false;
      this.accessToken = null;
      this.refreshToken = null;
      localStorage.removeItem("user");
    },
  },
});