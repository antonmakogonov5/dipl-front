import {defineStore} from 'pinia'
import {ethers} from 'ethers'
import {toRaw} from "vue";
import {useCustomFetch} from "@/composables/useCustomFetch";
import {useNativeFetch} from "~/composables/useNativeFetch.js";

export const useWalletsStore = defineStore('wallets', {
  state: () => ({
    isMetamaskInstalled: false,
    isMetamaskLoading: false,
    isMetamaskConnected: false,
    accounts: [],
    provider: null,
    polygonMainnetParams: {
      chainId: '0x13881', // Hexadecimal version of 80001 in decimal
      chainName: 'Mumbai Testnet',
      nativeCurrency: {
        name: 'MATIC',
        symbol: 'MATIC', // 2-6 characters long
        decimals: 18,
      },
      rpcUrls: ['https://matic-mumbai.chainstacklabs.com'],
      blockExplorerUrls: ['https://mumbai.polygonscan.com/'],
    }
  }),
  actions: {
    async connectToWallet() {
      if (window.ethereum) {
        try {
          this.isMetamaskLoading = true;
          const accs = await window.ethereum.request({method: 'eth_requestAccounts'});
          this.isMetamaskConnected = true;
          const provider = new ethers.BrowserProvider(window.ethereum, "any");

          this.accounts = await provider.listAccounts()

          this.provider = provider;
          const selectedAddress = window.ethereum.selectedAddress;
          const data = await useNativeFetch(`auth/sign/in?address=${selectedAddress}`)

          const message = 'I agree to the terms of use and confirm my identity to VineApp.';
          const fullMessage = `${message}\nNonce: ${data.nonce}`;
          const signature = await window.ethereum.request({
            method: 'personal_sign',
            params: [fullMessage, selectedAddress]
          });
          const userStore = useUserStore();
          await userStore.signIn(selectedAddress, signature);
        } catch (error) {
          console.error(error);
        } finally {
          this.isMetamaskLoading = false;
        }
      } else {
        console.error('Metamask not detected');
      }
    },
    async disconnectWallet() {
      this.isMetamaskConnected = false;
      this.accounts = [];
      this.provider = null;
    },
  },
})