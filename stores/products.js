import {defineStore} from "pinia";
import {useNativeFetch} from "~/composables/useNativeFetch.js";

export const useProductsStore = defineStore('products', {
  state: () => ({
    products: []
  }),
  actions: {
    async getProducts() {
      const data = await useNativeFetch('products', {}, {}, false);
      this.products = data;
    },
    async getProduct(id) {
      const data = await useNativeFetch(`products/${id}`);
      return data;
    },
    async editProductMetadata(id, form) {
      const formData = new FormData();
      formData.append('description', form.description);

      if (form.image) {
        formData.append('image', form.image);
      }
      const data = await useNativeFetch(`products/metadata/${id}`, {
        method: 'POST',
        body: formData,
      }, {}, true, true);
      return data;
    }
  }
});