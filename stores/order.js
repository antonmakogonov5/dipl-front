export const useOrderStore = defineStore('order', {
  state: () => ({
    order: {},
    orders: [],
    limit: 1,
  }),
  actions: {
    async createOrder(data, txHash) {
      data.txHash = txHash;
      const response = await useNativeFetch('orders', {
        method: 'POST',
        body: data,
      }, {}, true);
      return response;
    },
    async getOrders(page) {
      const response = await useNativeFetch(`orders?page=${page}&limit=${this.limit}`, {
        method: 'GET',
      }, {}, true);
      this.orders = response;
    },
    async getOrder(id) {
      const response = await useNativeFetch(`orders/${id}`, {
        method: 'GET',
      }, {}, true);
      return response;
    },
  }

})