import {useNativeFetch} from "~/composables/useNativeFetch.js";

export const useCartStore = defineStore('cart', {
  state: () => ({
    cartItems: [],
    productCount: 0,
  }),
  actions: {
    async addToCart(productId, quantity) {
      const response = await useNativeFetch('cart', {
        method: 'POST',
        body: {
          productId,
          quantity,
        },
      });
      this.productCount = response;
    },
    async getCartCount() {
      console.log('getCartCount');

      const resp = await useNativeFetch('cart/count');
      if (resp.hasOwnProperty('statusCode') && resp.statusCode === 401) {
        this.productCount = 0;
        return;
      }
      console.log(resp);
      this.productCount = resp;

    },
    async getCart() {
      const response = await useNativeFetch('cart');
      console.log(response);
      this.cartItems = response;
    },
    async changeQuantity(cartItem, quantity) {
      cartItem.quantity = cartItem.quantity + quantity;
      const response = await useNativeFetch(`cart/${cartItem.id}`, {
        method: 'PUT',
        body: {
          quantity: cartItem.quantity,
        },
      });
    },
    async deleteCartItem(cartItemId) {
      const response = await useNativeFetch(`cart/${cartItemId}`, {
        method: 'DELETE',
      });
      this.cartItems = this.cartItems.filter((item) => item.id !== cartItemId);
    },
  },
});
