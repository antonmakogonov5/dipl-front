import {useUserStore} from "~/stores/user";

export const useNativeFetch = async (request, opts, headers = {}, authorize = true, multipart=false) => {

  if (authorize) {
    const userStore = useUserStore();
    const {isAuthorized, accessToken} = storeToRefs(userStore);
    headers.Authorization = `Bearer ${accessToken?.value}`;
    console.log(accessToken.value);
  }

  const config = useRuntimeConfig();
  const url = config.public.apiBaseUrl + request

  if(!multipart) {
    headers = {
      'Content-Type': 'application/json',
      ...headers,
    };
    if(opts?.body) {
      opts.body = JSON.stringify(opts.body)
    }
  }

  const response = await fetch(url, {
    ...opts,
    headers: {
      ...headers,
    },
  });
  return response.json();
};
