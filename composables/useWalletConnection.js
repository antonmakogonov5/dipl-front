import {ref, reactive, toRaw} from 'vue';
import {ethers} from 'ethers';
import {useWalletsStore} from "@/stores/wallets";

export function useWalletConnection() {

    async function checkWalletConnection() {
        const metamaskStore = useWalletsStore()
        if (window.ethereum) {
            console.log(metamaskStore);
            metamaskStore.isMetamaskLoading = true;
            metamaskStore.isMetamaskInstalled = true;

            metamaskStore.provider.value = new ethers.BrowserProvider(window.ethereum, "any");

            try {
                await window.ethereum.request({
                    method: 'wallet_switchEthereumChain',
                    params: [{chainId: metamaskStore.polygonMainnetParams.chainId}],
                });
            } catch (switchError) {
                if (switchError.code === 4902) {
                    try {
                        await window.ethereum.request({
                            method: 'wallet_addEthereumChain',
                            params: [metamaskStore.polygonMainnetParams],
                        });
                    } catch (addError) {
                        console.error('Failed to add Polygon network:', addError);
                    }
                }
            }

            try {
                const accs = await toRaw(metamaskStore.provider).listAccounts();
                metamaskStore.accounts = accs;
                metamaskStore.isMetamaskConnected = accs.length > 0;
            } catch (error) {
                console.error('Failed to get accounts:', error);
            }

            metamaskStore.isMetamaskLoading = false;
        }
    }

    return {checkWalletConnection};
}
