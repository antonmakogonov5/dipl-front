import {useUserStore} from "~/stores/user";

export const useCustomFetch = (request, opts) => {
  const userStore = useUserStore();
  const {isAuthorized, accessToken} = storeToRefs(userStore);
  const config = useRuntimeConfig();
  console.log(isAuthorized.value)
  console.log(config.public.apiBaseUrl);
  // return  useFetch('http://localhost:3001/api/' + request
  // );
  return useFetch(request, {
    immediate: false,
    baseURL: config.public.apiBaseUrl,
    //TODO:
    // onResponse({ request, response, options }) {
    //   // console.log("USE CUSTOM FETCH", request, response, options);
    //   // Process the response data
    //   return response._data;
    // },
    // onResponseError({ request, response, options }) {
    //   // Handle the response errors
    // },
    // server: false,
    ...opts,
    headers: {
      // ...(isAuthorized.value && {Authorization: `Bearer ${accessToken.value}`}),
      ...opts?.headers,
    },
  });
};
